// homework18a.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>


class Data {
public:
    char name[20];
    int age;
};


class Stack1
{
private:
    int* arr;
    int Max;
    int index;

public:
    Stack1(int num)
    {
        Max = 100;
        if (num <= Max) 
            {
                arr = new int[num];
                Max = num;
            }
        }


        ~Stack1()
        {
            delete[] arr;
        }

        bool Push(int n)
        {
            if (index == Max)
                return false;
            else
            {
                arr[index] = n;
                index++;
                return true;

            }

        }

        int Pop()
        {
            if (index < 0)
                return 0;
            else {
                return arr[--index];
            }

        }

        bool Is_empty()
        {
            if (index <= 0)
                return true;
            else
                return false;

        }

        bool If_full()
        {
            if (index = Max)
                return true;
            else
                return false;
        }
    };



int main()
{
    Stack1 st(5);
    st.Push(2);
    st.Push(14);
    st.Push(3);

    std::cout << st.Pop() << '\n';

}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
